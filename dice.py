#! /usr/bin/env python3

import random, re

_sqlite = ""


def parse_message(message):
    print(message)
    for word in message.split():
        if not word.startswith("/r"):
            continue
        word = word.removeprefix("/r")

        result = sum(roll(r) for r in word.split("+"))
        message = message.replace(f"/r{word}", f"`{word}`->`{result}`", 1)
    return message


_roll_pattern = re.compile(r"(?P<num>[0-9]*)d(?P<mode>[bw]?)(?P<sides>[0-9]+)")
_roll_modes = {"": sum, "b": max, "w": min}


def roll(command):
    if command.isdigit():
        return int(command)

    num, mode, sides = _roll_pattern.fullmatch(command).groups()
    num = int(num) if num else 1

    return _roll_modes[mode](random.randint(1, int(sides)) for _ in range(num))


if __name__ == "__main__":
    print(parse_message("/rd20+d20+3"))
    print(parse_message("/rd20"))
    print(parse_message("/rd6"))
    print(parse_message("/r2db6"))
    print(parse_message("/r2dw6"))
