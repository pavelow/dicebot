#! /usr/bin/env python

# Discord Dice Bot
import asyncio, logging, discord, os, sys
import dice

logging.basicConfig(
    level=logging.INFO,
    force=True,
    format="%(asctime)-15s|%(name)-20s|%(levelname)-7s: %(message)s",
)


class bot(discord.Client):
    def __init__(self, channels, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.channels = {int(channel) for channel in channels.split("\n")}
        print(self.channels)

    async def on_ready(self):
        logging.info("Logged on as %s", self.user)

    async def on_message(self, message):
        if message.author == self.user:  # don't respond to ourselves
            return
        if message.channel.id not in self.channels:
            return

        if "/r" in message.content:
            msg = (
                "<@"
                + str(message.author.id)
                + "> Your rolls are:\n>>> "
                + message.content
            )
            msg = dice.parse_message(msg)
            await message.channel.send(msg)


if __name__ == "__main__":
    if "DISCORD_TOKEN" not in os.environ:
        print("Need API Token")
        sys.exit(0)

    client = bot(os.environ["CHANNELS"])
    client.run(os.environ["DISCORD_TOKEN"])
