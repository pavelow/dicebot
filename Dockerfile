FROM python
COPY . /dicebot
WORKDIR /dicebot
ENV CHANNELS ""
ENV DISCORD_TOKEN ""
RUN pip3 install pdm && pdm install
CMD pdm run ./dicebot.py
