# DiceBot

Discord dice rolling discord bot

## Commands
Commands are a single word starting with `/r`, and containing one or more of the following, separated
by + signs:

- Single dice: `d20` will roll a single 20-sided die. Any (integer) number of sides is supported
- Multiple dice: `5d20` will roll five 20-sided dice and give the sum of the results
- Best of n dice: `5db20` will roll five 20-sided dice and give the single best roll, discarding the others
- Worst of n dice: `5dw20` will roll five 20-sided dice and give the single worst roll, discarding the others
- Just a number: `20` will just be the number 20

Within a command, these are summed, so "1d6+3" is "roll 1 six-sided die and add three".

These can appear in ANY message on a configured channel, and ALL /r commands within a message will be
processed. The message will then be echod back to the channel with those commands replaced by their result. This facilitates including dicebot commands inline in, eg, D&D story text.

## Operation
Install `pdm` first via pip or pipx. Then,
- Install dependencies: `pdm install`
- Running: `pdm run ./dicebot.py`

### Environment Variables
- `DISCORD_TOKEN` (mandatory) contains the discord API token
- `CHANNELS` (optional) newline-separated IDs of channels for the bot to listen on

## Hacking
Install `pdm` and `pre-commit` (using pipx if you can or pip if you must).
After checkout do:
```
pdm install -d # Install dev dependencies
pdm run pre-commit install

```

pre-commit will automatically run some checks and fixer-uppers when you make a commit.
If it decides to change anything, you will need to re-stage the files and commit again.
