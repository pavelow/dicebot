import logging, parse, re, random, pytest
import dice

logging.basicConfig(
    level=logging.INFO,
    force=True,
    format="%(asctime)-15s|%(name)-20s|%(levelname)-7s: %(message)s",
)


@pytest.mark.parametrize("n_sides", [6, 20, 100, 1000])
def test_arb_single_dice(n_sides):
    message = dice.parse_message(f"/rd{n_sides}")

    assert re.match("`d[0-9]+`->`[0-9]+`", message)
    got_n_sides, result = parse.parse("`d{:d}`->`{:d}`", message)
    assert got_n_sides == n_sides
    assert result <= n_sides


@pytest.mark.parametrize("n_dice", [1, 3, 5, 10, 100])
@pytest.mark.parametrize("n_sides", [6, 20, 100, 1000])
def test_arb_multi_dice(n_dice, n_sides):
    message = dice.parse_message(f"/r{n_dice}d{n_sides}")

    assert re.match("`[0-9]+d[0-9]+`->`[0-9]+`", message)
    got_n_dice, got_n_sides, result = parse.parse("`{:d}d{:d}`->`{:d}`", message)
    assert got_n_sides == n_sides
    assert got_n_dice == n_dice
    assert result <= n_sides * n_dice


@pytest.mark.parametrize("command,min_,max_", [("/rd20+d20+3", 5, 43)])
def test_sum(command, min_, max_):
    message = dice.parse_message(command)
    returned_command, result = parse.parse("`{}`->`{:d}`", message)
    assert command.removeprefix("/r") == returned_command
    assert min_ <= result <= max_


@pytest.mark.parametrize("mode", "bw")
@pytest.mark.parametrize("n_dice", [1, 3, 5, 10, 100])
@pytest.mark.parametrize("n_sides", [6, 20, 100, 1000])
def test_multi_dice_modes(n_dice, n_sides, mode):
    command = f"/r{n_dice}d{mode}{n_sides}"
    message = dice.parse_message(command)
    returned_command, result = parse.parse("`{}`->`{:d}`", message)

    assert command.removeprefix("/r") == returned_command
    assert 1 <= result <= n_sides
